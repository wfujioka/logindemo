package com.logindemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import com.logindemo.filter.LoginFilter;
import com.logindemo.manager.LoginManager;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
@ComponentScan
@ServletComponentScan
public class LogindemoApplication {

  private @Autowired LoginManager loginManager;

  public static void main(String[] args) {
    SpringApplication.run(LogindemoApplication.class, args);
  }

  @Bean
  public FilterRegistrationBean<LoginFilter> loginFilter() {
    FilterRegistrationBean<LoginFilter> regBean = new FilterRegistrationBean<>();
    regBean.setFilter(new LoginFilter(loginManager));
    regBean.addUrlPatterns("/*");
    regBean.setOrder(1);
    return regBean;
  }

}
