package com.logindemo.adapter;

import java.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.wildfly.security.password.interfaces.BCryptPassword;
import com.logindemo.manager.PasswordManager;

@Component
public class PasswordAdapter {

  public static final int PASSWORD_ITERATIONS_DEFAULT = 10;

  private @Autowired PasswordManager passwordManager;

  private String getBcryptHash(int iterations, byte[] salt, char[] plainTextPassword) {
    BCryptPassword bCryptPassword =
        passwordManager.generateBCryptPassword(iterations, salt, plainTextPassword);
    byte[] hash = bCryptPassword.getHash();
    return Base64.getEncoder().encodeToString(hash);
  }

  public String encodePassword(Integer iterations, String encodedSalt, String plainTextPassword) {
    if (StringUtils.isEmpty(encodedSalt) || StringUtils.isEmpty(plainTextPassword)
        || iterations == null) {
      return null;
    }
    byte[] saltInBytes = Base64.getDecoder().decode(encodedSalt);
    char[] passwordInChars = plainTextPassword.toCharArray();
    return getBcryptHash(iterations, saltInBytes, passwordInChars);
  }


}
