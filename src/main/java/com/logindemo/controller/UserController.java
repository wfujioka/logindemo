package com.logindemo.controller;

import java.util.List;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.logindemo.entity.AppUser;
import com.logindemo.manager.UserManager;

@RestController
public class UserController {

  private @Autowired UserManager userManager;

  @GetMapping(value = "/users")
  @RolesAllowed("User")
  public List<AppUser> getPatient() {
    return userManager.getAllUsers();
  }

  @GetMapping(value = "/ping")
  public String ping() {
    return "pong";
  }
}
