package com.logindemo.entity;

public class AccessLog {

  private String username;
  private int userId;
  private String userAgent;
  private String referrer;
  private String hostName;
  private String serverHostName;
  private String method;
  private String sessID;
  private String requestUri;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public String getReferrer() {
    return referrer;
  }

  public void setReferrer(String referrer) {
    this.referrer = referrer;
  }

  public String getHostName() {
    return hostName;
  }

  public void setHostName(String hostName) {
    this.hostName = hostName;
  }

  public String getServerHostName() {
    return serverHostName;
  }

  public void setServerHostName(String serverHostName) {
    this.serverHostName = serverHostName;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public String getSessID() {
    return sessID;
  }

  public void setSessID(String sessID) {
    this.sessID = sessID;
  }

  public String getRequestUri() {
    return requestUri;
  }

  public void setRequestUri(String requestUri) {
    this.requestUri = requestUri;
  }
}
