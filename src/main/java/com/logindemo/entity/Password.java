package com.logindemo.entity;

public class Password {

  private String passwordHash;
  private String passwordSalt;
  private Integer passwordIterations;

  public String getPasswordHash() {
    return passwordHash;
  }

  public void setPasswordHash(String passwordHash) {
    this.passwordHash = passwordHash;
  }

  public String getPasswordSalt() {
    return passwordSalt;
  }

  public void setPasswordSalt(String passwordSalt) {
    this.passwordSalt = passwordSalt;
  }

  public Integer getPasswordIterations() {
    return passwordIterations;
  }

  public void setPasswordIterations(Integer passwordIterations) {
    this.passwordIterations = passwordIterations;
  }
}
