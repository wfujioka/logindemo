package com.logindemo.filter;

import java.io.IOException;
import java.security.Principal;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import com.logindemo.entity.AccessLog;
import com.logindemo.manager.LoginManager;

public class LoginFilter implements Filter {

  private LoginManager loginManager;

  public LoginFilter(LoginManager loginManager) {
    this.loginManager = loginManager;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {

    HttpServletRequest httpRequest = (HttpServletRequest) request;

    Principal userPrincipal = httpRequest.getUserPrincipal();
    String sessionId = httpRequest.getSession().getId();
    AccessLog accessLog = toAccessLog(userPrincipal, httpRequest);
    System.out
        .println("LoginFilter - userPrincipal: " + userPrincipal + " | sessionId: " + sessionId);
    loginManager.initLogin(userPrincipal, sessionId, accessLog);

    chain.doFilter(request, response);

  }

  private AccessLog toAccessLog(Principal userPrincipal, HttpServletRequest request) {

    String username = (userPrincipal == null) ? null : userPrincipal.getName();
    String userAgent = request.getHeader("User-Agent");
    String referrer = request.getHeader("Referer");
    String hostName = request.getRemoteHost();
    String serverHostName = request.getServerName();
    String method = request.getMethod();
    String sessID = request.getSession().getId();
    String requestUri = request.getRequestURI();

    AccessLog args = new AccessLog();
    args.setUsername(username);
    args.setUserAgent(userAgent);
    args.setReferrer(referrer);
    args.setHostName(hostName);
    args.setServerHostName(serverHostName);
    args.setMethod(method);
    args.setSessID(sessID);
    args.setRequestUri(requestUri);

    return args;
  }

  public void setLoginManager(LoginManager loginManager) {
    this.loginManager = loginManager;
  }

}
