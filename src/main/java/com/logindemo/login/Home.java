package com.logindemo.login;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class Home extends HttpServlet {
  private static final long serialVersionUID = 1L;

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    loadPage(response);
  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    request.getSession();
    loadPage(response);
  }

  private void loadPage(HttpServletResponse response) throws IOException {
    PrintWriter out = response.getWriter();
    response.setContentType("text/html");
    out.print("<html><body><h2>Home</h2><br/><br/>");
    out.print("<a href='/portal'>Tmds Portal</a>");
    out.print("<a href='/logindemo/logout'>Logout</a>");
    out.print("</body></html>");
  }
}
