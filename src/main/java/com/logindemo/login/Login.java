package com.logindemo.login;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/login")
public class Login extends HttpServlet {
  private static final long serialVersionUID = 1L;

  public Login() {
    super();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    renderLoginPage(response);
  }

  /**
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    renderLoginPage(response);
  }

  private void renderLoginPage(HttpServletResponse response) throws IOException {
    PrintWriter out = response.getWriter();
    response.setContentType("text/html");

    out.print("<html><body><h2>TMDS/MSAT Temp Login</h2>");
    out.print("<form method='post' action='j_security_check'>");
    out.print("<br>Username: <input type='text' name='j_username'/>");
    out.print("<br>Password: <input type='password' name='j_password'/>");
    out.print("<br><input type='submit' value='Login'/>");
    out.print("</form></html>");
  }

}
