package com.logindemo.login;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/login-fail")
public class LoginFail extends HttpServlet {
  private static final long serialVersionUID = 1L;

  public LoginFail() {
    super();
  }

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    renderLoginFailure(request, response);
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    renderLoginFailure(request, response);
  }

  private void renderLoginFailure(HttpServletRequest req, HttpServletResponse response)
      throws IOException {
    PrintWriter out = response.getWriter();
    response.setContentType("text/html");

    String userAgent = req.getHeader("User-Agent");
    // get the HTTP referrer
    String referrer = req.getHeader("Referer");
    // get the host name or IP
    String hostName = req.getRemoteHost();
    // get the server host name or IP
    String serverHostName = req.getServerName();
    // get the HTTP method
    String method = req.getMethod();
    // get the session ID
    String sessID = req.getSession().getId();

    out.print("<html><body><h2>TMDS/MSAT Temp Login</h2>");
    out.print("<div>Login fail, so taking you back to login page</div>");
    out.print(
        "<div>" + userAgent + referrer + hostName + serverHostName + method + sessID + "</div>");
    out.print("<form method='post' action='j_security_check'>");
    out.print("<br>Username: <input type='text' name='j_username'/>");
    out.print("<br>Password: <input type='password' name='j_password'/>");
    out.print("<br><input type='submit' value='Login'/>");
    out.print("</form></html>");
  }

}
