package com.logindemo.login;

import java.security.acl.Group;
import javax.security.auth.login.LoginException;
import org.jboss.security.SimpleGroup;
import org.jboss.security.SimplePrincipal;
import org.jboss.security.auth.spi.UsernamePasswordLoginModule;

public class TestCustomLoginModule extends UsernamePasswordLoginModule {

  @Override
  protected String getUsersPassword() throws LoginException {
    return "";
  }

  @Override
  protected boolean validatePassword(String inputPassword, String expectedPassword) {
    return true;
  }

  @Override
  protected Group[] getRoleSets() throws LoginException {
    SimpleGroup group = new SimpleGroup("Roles");
    group.addMember(new SimplePrincipal("User"));
    return new Group[] {group};
  }
}
