package com.logindemo.manager;

import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.logindemo.entity.AccessLog;
import com.logindemo.repo.AccessLogRepo;

@Service
public class LoginManager {

  private @Autowired AccessLogRepo accessLogRepo;

  public void initLogin(Principal principal, String sessionId, AccessLog accessLog) {
    System.out.println("LoginManager - principal: " + principal + " | sessionId: " + sessionId);
    if (principal == null || sessionId == null) {
      return;
    }
    // TODO: add stuff to initialize login...
    accessLogRepo.saveLogAccess(accessLog, true);
  }
}
