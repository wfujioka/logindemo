package com.logindemo.manager;

import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import org.springframework.stereotype.Service;
import org.wildfly.security.password.PasswordFactory;
import org.wildfly.security.password.interfaces.BCryptPassword;
import org.wildfly.security.password.spec.EncryptablePasswordSpec;
import org.wildfly.security.password.spec.IteratedSaltedPasswordAlgorithmSpec;

@Service
public class PasswordManager {

  public byte[] generateRandomSalt() {
    byte[] salt = new byte[BCryptPassword.BCRYPT_SALT_SIZE];
    SecureRandom random = new SecureRandom();
    random.nextBytes(salt);
    return salt;
  }

  public BCryptPassword generateBCryptPassword(int iterations, byte[] salt,
      char[] plainTextPassword) {

    PasswordFactory passwordFactory = getPasswordFactory();
    EncryptablePasswordSpec encryptableSpec =
        getEncryptablePasswordSpec(iterations, salt, plainTextPassword);

    BCryptPassword original = null;
    try {
      original = (BCryptPassword) passwordFactory.generatePassword(encryptableSpec);
    } catch (InvalidKeySpecException ie) {
      throw new RuntimeException("Problem Generating password with JBoss Security", ie);
    }
    return original;
  }

  /**
   * TODO Contact support about replacement for deprecated constructor
   */
  private PasswordFactory getPasswordFactory() {
    @SuppressWarnings("deprecation")
    final Provider elytronProvider = new org.wildfly.security.WildFlyElytronProvider();
    PasswordFactory passwordFactory = null;
    try {
      passwordFactory =
          PasswordFactory.getInstance(BCryptPassword.ALGORITHM_BCRYPT, elytronProvider);
    } catch (NoSuchAlgorithmException e) {
      throw new RuntimeException("Problem getting password factory with JBoss Security", e);
    }
    return passwordFactory;
  }

  private EncryptablePasswordSpec getEncryptablePasswordSpec(int iterations, byte[] salt,
      char[] plainTextPassword) {
    return new EncryptablePasswordSpec(plainTextPassword,
        getIteratedSaltedPasswordAlgorithmSpec(iterations, salt));
  }

  private IteratedSaltedPasswordAlgorithmSpec getIteratedSaltedPasswordAlgorithmSpec(int iterations,
      byte[] salt) {
    return new IteratedSaltedPasswordAlgorithmSpec(iterations, salt);
  }
}
