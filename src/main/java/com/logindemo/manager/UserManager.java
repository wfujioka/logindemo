package com.logindemo.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.logindemo.entity.AppUser;
import com.logindemo.repo.UserRepo;
import com.logindemo.repo.UserRolesRepo;

@Service
public class UserManager {

  private UserRepo userRepo;
  private UserRolesRepo userRolesRepo;
  private Map<String, AppUser> map;

  public UserManager(UserRepo userRepo, UserRolesRepo userRolesRepo) {
    this.userRepo = userRepo;
    this.userRolesRepo = userRolesRepo;
    populateAppUsersMap();
  }

  private void populateAppUsersMap() {
    map = new HashMap<>();
    for (AppUser user : userRepo.getAllUsers()) {
      user.setUserRolesList(userRolesRepo.getRolesForUser(user.getLoginName()));
      map.put(user.getLoginName(), user);
    }
  }

  public AppUser getUser(String username) {
    return map.get(username);
  }

  public List<AppUser> getAllUsers() {
    return new ArrayList<>(map.values());
  }

}
