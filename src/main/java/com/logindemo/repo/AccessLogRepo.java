package com.logindemo.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.logindemo.entity.AccessLog;

@Repository
public class AccessLogRepo {

  private @Autowired JdbcTemplate jdbcTemplate;

  private String getBrowserInfo(AccessLog accessLog) {
    String userAgent = accessLog.getUserAgent();
    return userAgent.substring(0, userAgent.length() > 255 ? 255 : userAgent.length());
  }

  private String getHostName(AccessLog accessLog) {
    String hostName = accessLog.getHostName();
    return hostName.substring(0, hostName.length() > 150 ? 150 : hostName.length());
  }

  private String getServerHostName(AccessLog accessLog) {
    String serverHostName = accessLog.getServerHostName();
    return serverHostName.substring(0,
        serverHostName.length() > 150 ? 150 : serverHostName.length());
  }

  private String getFailedLogin(boolean isLoginSuccessful) {
    return isLoginSuccessful ? "N" : "Y";
  }

  public void saveLogAccess(AccessLog accessLog, boolean loginSuccess) {

    String sql = "INSERT INTO MSA.ACCESS_LOG "
        + "(USER_ID, URL, LOGIN_NAME, BROWSER, HOST_NAME, SERVER_HOST_NAME, SESSION_ID, FAILED_LOGIN, REQUEST_METHOD, REFERRER) "
        + "VALUES (?,?,?,?,?,?,?,?,?,?)";

    Object[] params = new Object[10];
    params[0] = accessLog.getUserId();
    params[1] = accessLog.getRequestUri();
    params[2] = accessLog.getUsername();
    params[3] = getBrowserInfo(accessLog);
    params[4] = getHostName(accessLog);
    params[5] = getServerHostName(accessLog);
    params[6] = accessLog.getSessID();
    params[7] = getFailedLogin(loginSuccess);
    params[8] = accessLog.getMethod();
    params[9] = accessLog.getReferrer();

    int resultCount = jdbcTemplate.update(sql, params);

    System.out.println("saveLogAccess: " + resultCount + " rows added");
  }
}
