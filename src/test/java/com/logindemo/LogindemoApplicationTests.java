package com.logindemo;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("boot")
class LogindemoApplicationTests {

  @Test
  void contextLoads() {}

}
