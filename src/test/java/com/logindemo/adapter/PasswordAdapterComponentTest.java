package com.logindemo.adapter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("boot")
public class PasswordAdapterComponentTest {

  private @Autowired PasswordAdapter passwordAdapter;

  @Test
  public void testEncodePassword() {
    String encodedSalt = "5VgFMHsZ/lRbT1JcXZTM1g==";
    Integer iterations = 10;
    String clearTextPassword = "password";
    String expected = "jYz9LuOf2LFvMCVr8MYXCP6JW/l/iUY=";

    String actual = passwordAdapter.encodePassword(iterations, encodedSalt, clearTextPassword);

    assertEquals(expected, actual);
  }

}
