package com.logindemo.repo;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import com.logindemo.entity.AppUser;

@SpringBootTest
@ActiveProfiles("boot")
public class UserRepoComponentTest {

  private @Autowired UserRepo userRepo;

  @Test
  public void getUsers_VerifyUsersAreBeingReturned() {
    List<AppUser> usersList = userRepo.getAllUsers();

    assertNotNull(usersList);
    assertTrue(usersList.size() > 0);
  }

}
